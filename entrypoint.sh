#!/bin/bash

sed -i "s/DB_HOST/$DB_HOST/g" /download/mirrorbrain.conf
sed -i "s/DB_PORT/$DB_PORT/g" /download/mirrorbrain.conf
sed -i "s/DB_NAME/$DB_NAME/g" /download/mirrorbrain.conf
sed -i "s/DB_USER/$DB_USER/g" /download/mirrorbrain.conf
sed -i "s/DB_PASSWORD/$DB_PASSWORD/g" /download/mirrorbrain.conf

sed -i "s/DB_HOST/$DB_HOST/g" /download/httpd.conf
sed -i "s/DB_PORT/$DB_PORT/g" /download/httpd.conf
sed -i "s/DB_NAME/$DB_NAME/g" /download/httpd.conf
sed -i "s/DB_USER/$DB_USER/g" /download/httpd.conf
sed -i "s/DB_PASSWORD/$DB_PASSWORD/g" /download/httpd.conf

exec httpd -DFOREGROUND $@
 
