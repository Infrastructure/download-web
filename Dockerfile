FROM centos/httpd

USER root
RUN yum -y update && yum -y install epel-release && \
    yum -y install mirrorbrain mirrorbrain-tools mirrorbrain-doc \
	mirrorbrain-scanner mod_autoindex_mb mod_mirrorbrain mod_form \
	perl-IO-Socket-SSL perl-LWP-Protocol-https vim && yum clean all

RUN sed -ri ' s!^(\s*CustomLog)\s+\S+!\1 /proc/self/fd/1!g; s!^(\s*ErrorLog)\s+\S+!\1 /proc/self/fd/2!g;' /etc/httpd/conf/httpd.conf
RUN sed -i 's/Listen\ 80/Listen\ 8080/' /etc/httpd/conf/httpd.conf
RUN sed -i '/mod_autoindex.so/d' /etc/httpd/conf.modules.d/00-base.conf
RUN sed -i '/mod_auth_digest.so/d' /etc/httpd/conf.modules.d/00-base.conf
RUN echo 'PidFile /download/httpd.pid' >> /etc/httpd/conf/httpd.conf
RUN rm -f /etc/httpd/conf.d/welcome.conf

RUN ln -sf /download/httpd.conf /etc/httpd/conf.d/download.conf
RUN ln -sf /download/mirrorbrain.conf /etc/mirrorbrain.conf
ADD entrypoint.sh /entrypoint.sh

RUN groupadd download -g 1000550000  && \
    useradd download -g 1000550000 -u 1000550000 -G apache -r -l -m  && \
    install -d -o download -g download /download/data

ADD download.gnome.org.conf /download/httpd.conf
ADD mirrorbrain.conf /download/mirrorbrain.conf
RUN chown -R download:download /download

RUN sed -i 's/d.adsrc/pg_catalog.pg_get_expr(d.adbin, d.adrelid)/' \
        /usr/lib/python2.7/site-packages/sqlobject/postgres/pgconnection.py

USER download

ENTRYPOINT ["/entrypoint.sh"]
